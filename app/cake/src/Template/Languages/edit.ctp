<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Language $language
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($language) ?>
            <legend class="capital-heading"><?= __('Edit Language') ?></legend>
            <?php
                echo $this->Form->control('sequence');
                echo $this->Form->control('parent_id', ['options' => $parentLanguages, 'empty' => true]);
                echo $this->Form->control('language');
                echo $this->Form->control('protocol_code');
                echo $this->Form->control('inline_code');
                echo $this->Form->control('notes');
                echo $this->Form->control('artifacts._ids', ['options' => $artifacts]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $language->id],
                ['class' => 'btn-action'],
                ['confirm' => __('Are you sure you want to delete # {0}?', $language->id)]
            )
        ?>
        <br/>
        <?= $this->Html->link(__('List Languages'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Parent Languages'), ['controller' => 'Languages', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Parent Language'), ['controller' => 'Languages', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
