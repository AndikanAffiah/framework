<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CdlNote[]|\Cake\Collection\CollectionInterface $cdlNotes
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Cdl Note'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Cdl Notes') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year') ?></th>
            <th scope="col"><?= $this->Paginator->sort('number') ?></th>
            <th scope="col"><?= $this->Paginator->sort('author_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('uploaded') ?></th>
            <th scope="col"><?= $this->Paginator->sort('preprint') ?></th>
            <th scope="col"><?= $this->Paginator->sort('archival') ?></th>
            <th scope="col"><?= $this->Paginator->sort('preprint_state') ?></th>
            <th scope="col"><?= $this->Paginator->sort('online') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($cdlNotes as $cdlNote): ?>
        <tr>
            <td><?= $this->Number->format($cdlNote->id) ?></td>
            <td><?= h($cdlNote->year) ?></td>
            <td><?= $this->Number->format($cdlNote->number) ?></td>
            <td><?= $cdlNote->has('author') ? $this->Html->link($cdlNote->author->id, ['controller' => 'Authors', 'action' => 'view', $cdlNote->author->id]) : '' ?></td>
            <td><?= h($cdlNote->uploaded) ?></td>
            <td><?= h($cdlNote->preprint) ?></td>
            <td><?= h($cdlNote->archival) ?></td>
            <td><?= h($cdlNote->preprint_state) ?></td>
            <td><?= h($cdlNote->online) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $cdlNote->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $cdlNote->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $cdlNote->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $cdlNote->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

