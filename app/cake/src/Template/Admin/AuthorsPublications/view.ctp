<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AuthorsPublication $authorsPublication
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Authors Publication') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Publication') ?></th>
                    <td><?= $authorsPublication->has('publication') ? $this->Html->link($authorsPublication->publication->title, ['controller' => 'Publications', 'action' => 'view', $authorsPublication->publication->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Author') ?></th>
                    <td><?= $authorsPublication->has('author') ? $this->Html->link($authorsPublication->author->id, ['controller' => 'Authors', 'action' => 'view', $authorsPublication->author->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($authorsPublication->id) ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Authors Publication'), ['action' => 'edit', $authorsPublication->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Authors Publication'), ['action' => 'delete', $authorsPublication->id], ['confirm' => __('Are you sure you want to delete # {0}?', $authorsPublication->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Authors Publications'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Authors Publication'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



