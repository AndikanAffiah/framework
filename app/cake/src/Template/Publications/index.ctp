<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication[]|\Cake\Collection\CollectionInterface $publications
 */
?>
<div class="boxed mx-0">
    <div class="capital-heading"><?= __('Related Actions') ?></div>

        <?= $this->Html->link(__('New Publication'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Entry Types'), ['controller' => 'EntryTypes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Entry Type'), ['controller' => 'EntryTypes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Journals'), ['controller' => 'Journals', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Journal'), ['controller' => 'Journals', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Abbreviations'), ['controller' => 'Abbreviations', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Abbreviation'), ['controller' => 'Abbreviations', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>

</div>

<h3 class="display-4 pt-3"><?= __('Publications') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('year') ?></th>
            <th scope="col"><?= $this->Paginator->sort('entry_type_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('address') ?></th>
            <th scope="col"><?= $this->Paginator->sort('annote') ?></th>
            <th scope="col"><?= $this->Paginator->sort('book_title') ?></th>
            <th scope="col"><?= $this->Paginator->sort('chapter') ?></th>
            <th scope="col"><?= $this->Paginator->sort('crossref') ?></th>
            <th scope="col"><?= $this->Paginator->sort('edition') ?></th>
            <th scope="col"><?= $this->Paginator->sort('editor') ?></th>
            <th scope="col"><?= $this->Paginator->sort('how_published') ?></th>
            <th scope="col"><?= $this->Paginator->sort('institution') ?></th>
            <th scope="col"><?= $this->Paginator->sort('journal_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('month') ?></th>
            <th scope="col"><?= $this->Paginator->sort('note') ?></th>
            <th scope="col"><?= $this->Paginator->sort('number') ?></th>
            <th scope="col"><?= $this->Paginator->sort('organization') ?></th>
            <th scope="col"><?= $this->Paginator->sort('pages') ?></th>
            <th scope="col"><?= $this->Paginator->sort('publisher') ?></th>
            <th scope="col"><?= $this->Paginator->sort('school') ?></th>
            <th scope="col"><?= $this->Paginator->sort('title') ?></th>
            <th scope="col"><?= $this->Paginator->sort('volume') ?></th>
            <th scope="col"><?= $this->Paginator->sort('abbreviation_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('series') ?></th>
            <th scope="col"><?= $this->Paginator->sort('oclc') ?></th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($publications as $publication): ?>
        <tr>
            <td><?= $this->Number->format($publication->id) ?></td>
            <td><?= h($publication->year) ?></td>
            <td><?= $publication->has('entry_type') ? $this->Html->link($publication->entry_type->title, ['controller' => 'EntryTypes', 'action' => 'view', $publication->entry_type->id]) : '' ?></td>
            <td><?= h($publication->address) ?></td>
            <td><?= h($publication->annote) ?></td>
            <td><?= h($publication->book_title) ?></td>
            <td><?= h($publication->chapter) ?></td>
            <td><?= h($publication->crossref) ?></td>
            <td><?= h($publication->edition) ?></td>
            <td><?= h($publication->editor) ?></td>
            <td><?= h($publication->how_published) ?></td>
            <td><?= h($publication->institution) ?></td>
            <td><?= $publication->has('journal') ? $this->Html->link($publication->journal->id, ['controller' => 'Journals', 'action' => 'view', $publication->journal->id]) : '' ?></td>
            <td><?= h($publication->month) ?></td>
            <td><?= h($publication->note) ?></td>
            <td><?= h($publication->number) ?></td>
            <td><?= h($publication->organization) ?></td>
            <td><?= h($publication->pages) ?></td>
            <td><?= h($publication->publisher) ?></td>
            <td><?= h($publication->school) ?></td>
            <td><?= h($publication->title) ?></td>
            <td><?= h($publication->volume) ?></td>
            <td><?= $publication->has('abbreviation') ? $this->Html->link($publication->abbreviation->id, ['controller' => 'Abbreviations', 'action' => 'view', $publication->abbreviation->id]) : '' ?></td>
            <td><?= h($publication->series) ?></td>
            <td><?= $this->Number->format($publication->oclc) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $publication->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $publication->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $publication->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $publication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

