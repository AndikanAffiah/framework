<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading text-center">Login</div>
        <?= $this->Flash->render() ?>

        <?= $this->Form->create() ?>
            <?= $this->Form->control('username') ?>
            <?= $this->Form->control('password') ?>

            <?= $this->Form->submit(); ?>
        <?= $this->Form->end() ?>
    </div>

</div>
