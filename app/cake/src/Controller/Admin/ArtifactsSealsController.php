<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ArtifactsSeals Controller
 *
 * @property \App\Model\Table\ArtifactsSealsTable $ArtifactsSeals
 *
 * @method \App\Model\Entity\ArtifactsSeal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsSealsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts']
        ];
        $artifactsSeals = $this->paginate($this->ArtifactsSeals);

        $this->set(compact('artifactsSeals'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Seal id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsSeal = $this->ArtifactsSeals->get($id, [
            'contain' => ['Artifacts']
        ]);

        $this->set('artifactsSeal', $artifactsSeal);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsSeal = $this->ArtifactsSeals->newEntity();
        if ($this->request->is('post')) {
            $artifactsSeal = $this->ArtifactsSeals->patchEntity($artifactsSeal, $this->request->getData());
            if ($this->ArtifactsSeals->save($artifactsSeal)) {
                $this->Flash->success(__('The artifacts seal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts seal could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsSeals->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsSeal', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Seal id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsSeal = $this->ArtifactsSeals->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsSeal = $this->ArtifactsSeals->patchEntity($artifactsSeal, $this->request->getData());
            if ($this->ArtifactsSeals->save($artifactsSeal)) {
                $this->Flash->success(__('The artifacts seal has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts seal could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsSeals->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsSeal', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Seal id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsSeal = $this->ArtifactsSeals->get($id);
        if ($this->ArtifactsSeals->delete($artifactsSeal)) {
            $this->Flash->success(__('The artifacts seal has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts seal could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
