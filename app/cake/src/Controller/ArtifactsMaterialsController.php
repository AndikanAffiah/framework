<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsMaterials Controller
 *
 * @property \App\Model\Table\ArtifactsMaterialsTable $ArtifactsMaterials
 *
 * @method \App\Model\Entity\ArtifactsMaterial[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsMaterialsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts', 'Materials', 'MaterialColors', 'MaterialAspects']
        ];
        $artifactsMaterials = $this->paginate($this->ArtifactsMaterials);

        $this->set(compact('artifactsMaterials'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Material id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsMaterial = $this->ArtifactsMaterials->get($id, [
            'contain' => ['Artifacts', 'Materials', 'MaterialColors', 'MaterialAspects']
        ]);

        $this->set('artifactsMaterial', $artifactsMaterial);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsMaterial = $this->ArtifactsMaterials->newEntity();
        if ($this->request->is('post')) {
            $artifactsMaterial = $this->ArtifactsMaterials->patchEntity($artifactsMaterial, $this->request->getData());
            if ($this->ArtifactsMaterials->save($artifactsMaterial)) {
                $this->Flash->success(__('The artifacts material has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts material could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsMaterials->Artifacts->find('list', ['limit' => 200]);
        $materials = $this->ArtifactsMaterials->Materials->find('list', ['limit' => 200]);
        $materialColors = $this->ArtifactsMaterials->MaterialColors->find('list', ['limit' => 200]);
        $materialAspects = $this->ArtifactsMaterials->MaterialAspects->find('list', ['limit' => 200]);
        $this->set(compact('artifactsMaterial', 'artifacts', 'materials', 'materialColors', 'materialAspects'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Material id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsMaterial = $this->ArtifactsMaterials->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsMaterial = $this->ArtifactsMaterials->patchEntity($artifactsMaterial, $this->request->getData());
            if ($this->ArtifactsMaterials->save($artifactsMaterial)) {
                $this->Flash->success(__('The artifacts material has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts material could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsMaterials->Artifacts->find('list', ['limit' => 200]);
        $materials = $this->ArtifactsMaterials->Materials->find('list', ['limit' => 200]);
        $materialColors = $this->ArtifactsMaterials->MaterialColors->find('list', ['limit' => 200]);
        $materialAspects = $this->ArtifactsMaterials->MaterialAspects->find('list', ['limit' => 200]);
        $this->set(compact('artifactsMaterial', 'artifacts', 'materials', 'materialColors', 'materialAspects'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Material id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsMaterial = $this->ArtifactsMaterials->get($id);
        if ($this->ArtifactsMaterials->delete($artifactsMaterial)) {
            $this->Flash->success(__('The artifacts material has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts material could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
