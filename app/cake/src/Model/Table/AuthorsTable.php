<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Authors Model
 *
 * @property \App\Model\Table\CdlNotesTable|\Cake\ORM\Association\HasMany $CdlNotes
 * @property \App\Model\Table\CreditsTable|\Cake\ORM\Association\HasMany $Credits
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\HasMany $Users
 * @property \App\Model\Table\PublicationsTable|\Cake\ORM\Association\BelongsToMany $Publications
 *
 * @method \App\Model\Entity\Author get($primaryKey, $options = [])
 * @method \App\Model\Entity\Author newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Author[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Author|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Author patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Author[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Author findOrCreate($search, callable $callback = null, $options = [])
 */
class AuthorsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('authors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('CdlNotes', [
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('Credits', [
            'foreignKey' => 'author_id'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'author_id'
        ]);
        $this->belongsToMany('Publications', [
            'foreignKey' => 'author_id',
            'targetForeignKey' => 'publication_id',
            'joinTable' => 'authors_publications'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('author')
            ->maxLength('author', 50)
            ->requirePresence('author', 'create')
            ->notEmpty('author');

        return $validator;
    }
}
