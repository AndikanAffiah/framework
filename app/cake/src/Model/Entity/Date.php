<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Date Entity
 *
 * @property int $id
 * @property string|null $day_no
 * @property string|null $day_remarks
 * @property int|null $month_id
 * @property bool|null $is_uncertain
 * @property string|null $month_no
 * @property int|null $year_id
 * @property int|null $dynasty_id
 * @property int|null $ruler_id
 * @property string|null $absolute_year
 *
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Year $year
 * @property \App\Model\Entity\Dynasty $dynasty
 * @property \App\Model\Entity\Ruler $ruler
 * @property \App\Model\Entity\Artifact[] $artifacts
 */
class Date extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'day_no' => true,
        'day_remarks' => true,
        'month_id' => true,
        'is_uncertain' => true,
        'month_no' => true,
        'year_id' => true,
        'dynasty_id' => true,
        'ruler_id' => true,
        'absolute_year' => true,
        'month' => true,
        'year' => true,
        'dynasty' => true,
        'ruler' => true,
        'artifacts' => true
    ];
}
